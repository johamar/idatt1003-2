package edu.ntnu.stud;

import javax.xml.parsers.ParserConfigurationException;
import java.text.ParseException;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class UserMenu {
  private static TrainSchedule schedule;
  private static final int DISPLAY_BOARD = 1;
  private static final int ADD_DEPARTURE = 2;
  private static final int ASSIGN_TRACK = 3;
  private static final int ADD_DELAY = 4;
  private static final int SEARCH_BY_TRAIN_NR = 5;
  private static final int SEARCH_BY_DESTINATION = 6;
  private static final int UPDATE_CLOCK = 7;
  private static final int EXIT = 8;

  private int showMenu() {
    int menuChoice = 0;
    System.out.println("---------Togavgang Applikasjon V0.1---------\n" +
      "1: Vis informsajonstavle\n" + //Sortert etter avgangstidspunkt
      "2: Legge inn ny togavgang\n" + //Skal ikke være mulig å legge inn allerede eksisterende tognummer
      "3: Tildele spor til en togavgang\n" + //søk først etter tognummer, sett så spor
      "4: Legg inn forsinkelse på en togavgang\n" + // søk først etter tognummer, legg så til forsinkelse
      "5: Søk etter togavgang basert på tognummer\n" +
      "6: Søk etter togavgang basert på destinasjon\n" +
      "7: Oppdater klokken\n" +
      "8: Avslutt\n" +
      "--------------------------------------------\n" +
      "Vennligst velg et nummer mellom 1 og 8");
    Scanner scan = new Scanner(System.in);
    if (scan.hasNextInt()) {
      menuChoice = scan.nextInt();
    } else {
      System.out.println("Vennligst skriv et tall, ikke tekst");
    }
    return menuChoice;
  }
  public void init() {
    schedule = new TrainSchedule(LocalTime.parse("05:12"));

    TrainDeparture d1 = new TrainDeparture(LocalTime.parse("08:40"), "T5", 132, "OSLO", 2, LocalTime.parse("00:02"));
    TrainDeparture d2 = new TrainDeparture(LocalTime.parse("06:15"), "A1", 128, "OSLO", 2, LocalTime.parse("00:00"));
    TrainDeparture d3 = new TrainDeparture(LocalTime.parse("07:25"), "A7", 210, "TRONDHEIM", 5, LocalTime.parse("00:01"));
    TrainDeparture d4 = new TrainDeparture(LocalTime.parse("12:55"), "B6", 568, "BODØ", 8, LocalTime.parse("00:04"));
    TrainDeparture d5 = new TrainDeparture(LocalTime.parse("16:30"), "T5", 138, "OSLO", 3, LocalTime.parse("00:00"));
    TrainDeparture d6 = new TrainDeparture(LocalTime.parse("08:20"), "A7", 212, "TRONDHEIM", 6, LocalTime.parse("00:02"));

    schedule.addDeparture(d1);
    schedule.addDeparture(d2);
    schedule.addDeparture(d3);
    schedule.addDeparture(d4);
    schedule.addDeparture(d5);
    schedule.addDeparture(d6);
  }
  public void start() {


    boolean finished = false;
    Scanner scan = new Scanner(System.in);

    while (!finished) {
      int menuChoice = this.showMenu();
      switch (menuChoice) {
        case DISPLAY_BOARD:
          System.out.println("Informasjonstavlen:");
          System.out.println("                            " + schedule.getTime());
          System.out.println("-----------------------------------------------------------------");
          System.out.println("Avgangstid | Linje | Tognummer | Destinasjon | Spor | Forsinkelse");
          System.out.println(schedule.displayBoard());
          System.out.println("-----------------------------------------------------------------");
          break;
        case ADD_DEPARTURE:
          System.out.println("--------------------------------------------");
          System.out.println("Ny togavgang:");
          System.out.println("Skriv inn avgangstid (tt:mm):");
          String departureTime = scan.nextLine();
          System.out.println("Skriv inn linje:");
          String line = scan.nextLine();
          System.out.println("Skriv inn tognummer:");
          int trainNr = Integer.parseInt(scan.nextLine());
          System.out.println("Skriv inn destinasjon:");
          String destination = scan.nextLine().toUpperCase();
          System.out.println("Skriv inn spor (hvis ingen, skriv -1):");
          int track = Integer.parseInt(scan.nextLine());
          System.out.println("Skriv inn forsinkelse (tt:mm) (hvis ingen, trykk enter):");
          String delay = scan.nextLine();

          if (delay.isEmpty() || delay.equals("0000") || delay.equals("0")) {
            delay = "00:00";
          }

          try {
            TrainDeparture newDep = new TrainDeparture(LocalTime.parse(departureTime), line, trainNr, destination, track, LocalTime.parse(delay));
            if (schedule.addDeparture(newDep)) {
              schedule.addDeparture(newDep);
              System.out.println("Avgang lagt til!");
            } else {
              System.out.println("Denne avgangen finnes allerede");
            }
          } catch (DateTimeParseException e) {
            System.out.println("Tidspunkt må skrives som tt:mm (eks. 12:00)");
          } catch (NumberFormatException e) {
            System.out.println("Tognummer og spor må være heltall");
          } catch (Exception e) {
            System.out.println("Noe gikk galt, vennligst prøv igjen!");
          }



          break;
        case ASSIGN_TRACK:
          System.out.println("--------------------------------------------");
          System.out.println("Tildel spor");
          System.out.println("Skriv inn tognummer:");
          int trainNrTrack = scan.nextInt();
          System.out.println("Du skifter nå spor for tognumer " + trainNrTrack);
          System.out.println("Skriv inn spor for denne togavgangen: ");
          int newTrack = scan.nextInt();

          schedule.assignTrack(trainNrTrack, newTrack);
          System.out.println("Nytt spor tildelt " + trainNrTrack + " til "
                  + schedule.searchDepartureByNr(trainNrTrack).getDestination());

          break;
        case ADD_DELAY:
          System.out.println("--------------------------------------------");
          System.out.println("Legg til forsinkelse");
          System.out.println("Skriv inn tognummer");
          int trainNrDelay = Integer.parseInt(scan.nextLine());
          System.out.println("Du legger nå til forsinkelse for tognummer " + trainNrDelay);
          System.out.println("Skriv inn forsinkelse (hh:mm):");
          String delayAdded = scan.nextLine();
          schedule.addDelay(trainNrDelay, delayAdded);
          System.out.println("Forsinkelse lagt til for " + trainNrDelay);
          break;
        case SEARCH_BY_TRAIN_NR:
          System.out.println("--------------------------------------------");
          System.out.println("Søk etter togavgang basert på tognummer");
          System.out.println("Skriv inn tognummer:");
          int trainNrSearched = scan.nextInt();
          System.out.println("Togavgang med tognummer " + trainNrSearched + ":");
          System.out.println(schedule.searchDepartureByNr(trainNrSearched));
          break;
        case SEARCH_BY_DESTINATION:
          System.out.println("--------------------------------------------");
          System.out.println("Søk etter togavgang basert på destiasjon");
          System.out.println("Skriv inn destinasjon:");
          String trainDestSearched = scan.nextLine();
          if (!schedule.searchDepartureByDest(trainDestSearched).isEmpty()) {
            System.out.println("Togavganger som går til " + trainDestSearched + ":");
            System.out.println("                            " + schedule.getTime());
            System.out.println("-----------------------------------------------------------------");
            System.out.println("Avgangstid | Linje | Tognummer | Destinasjon | Spor | Forsinkelse");
            System.out.println(schedule.searchDepartureByDest(trainDestSearched));
          } else {
            System.out.println("Det finnes ingen togavganger til " + trainDestSearched);
          }
          break;
        case UPDATE_CLOCK:
          System.out.println("--------------------------------------------");
          System.out.println("Oppdater klokken");
          System.out.println("Skriv inn nytt klokkeslett (hh:mm):");
          String newTime = scan.nextLine();
          if (schedule.setTime(newTime)) {
            schedule.setTime(newTime);
            System.out.println("Ny tid registrert! Klokken er nå " + schedule.getTime());
          } else {
            System.out.println("Ugyldig tidspunkt! Du kan ikke stille klokken tilbake i tid.");
          }
          break;
        case EXIT:
          System.out.println("Avluttet!");
          return;
        default:
          System.out.println("Ugyldig valg, prøv igjen!");
          break;

      }


    }
  }
}
